from django import template
from kplugins.core.utils import HolderUtils, HolderRender


register = template.Library()


# @register.inclusion_tag('kplugins/includes/plugins.html', takes_context=True)
# def plugin_holder(context, site, identifier, holder_plugin_pks=None):
#     request = context['request']
#     holder_plugin_pks = holder_plugin_pks or []
#
#     return {
#         'plugins': HolderUtils(site, request).prepare(identifier, holder_plugin_pks=holder_plugin_pks)
#     }


@register.simple_tag(takes_context=True)
def kplugins_render_html(context, plugin_info):
    return HolderRender(context['request']).render_html(plugin_info)


@register.simple_tag(takes_context=True)
def kplugins_render_media(context, plugin_info):
    return HolderRender(context['request']).render_media(plugin_info)
