from django.contrib import admin

from kplugins.models import Plugin, Holder, HolderPlugin
from kplugins.forms import HolderPluginInlineForm


@admin.register(Plugin)
class PluginAdmin(admin.ModelAdmin):
    list_display = ('identifier', 'name', 'template_name', 'is_active')
    list_filter = ('site', 'module_path', 'is_active',)
    search_fields = ['identifier', 'name']


class HolderPluginInline(admin.TabularInline):
    model = HolderPlugin
    extra = 1
    fields = ['plugin', 'layout__grid', 'layout__tag_class', 'sort', 'is_active']
    readonly_fields = ['options']
    form = HolderPluginInlineForm


@admin.register(Holder)
class HolderAdmin(admin.ModelAdmin):
    list_display = ('identifier', 'is_active', 'site')
    list_filter = ('site', 'is_active',)
    search_fields = ['identifier',]
    fields = ['identifier']
    inlines = [HolderPluginInline]
