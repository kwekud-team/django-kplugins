import kplugins
from kplugins.contrib.plugins.layout import SpacerPlugin


module_path = 'kplugins'

kplugins.register(module_path, SpacerPlugin)
