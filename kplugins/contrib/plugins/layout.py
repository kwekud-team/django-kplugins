import kplugins


class SpacerPlugin(kplugins.BasePlugin):
    template_name = 'kplugins/plugins/spacer.html'
    name = 'Spacer'

    def get_context(self):
        return {}
