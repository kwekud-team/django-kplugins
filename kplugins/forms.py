from django import forms
from kplugins.models import HolderPlugin


class HolderPluginInlineForm(forms.ModelForm):
    layout__grid = forms.ChoiceField(required=False, choices=[], label='Layout/Grid')
    layout__tag_class = forms.CharField(required=False)

    class Meta:
        model = HolderPlugin
        fields = '__all__'

    def __init__(self, **kwargs):
        super().__init__(**kwargs)

        grid_xs = [('', '---')]
        grid_xs.extend([(str(x), x) for x in range(1, 13)])
        self.fields['layout__grid'].choices = grid_xs

        self._set_init_values()

    def _set_init_values(self):
        for field in self.fields:
            if '__' in field and self.instance.pk:
                self.fields[field].initial = self.instance.options.get(field, '')

    def save(self, commit=True):

        current_options = self.instance.options
        for field in self.fields:
            if '__' in field:
                val = self.cleaned_data[field]
                current_options[field] = val
        self.instance.options = current_options

        return super().save(commit=commit)
