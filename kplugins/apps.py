from django.apps import AppConfig


class CustomConfig(AppConfig):
    name = 'kplugins'

    def ready(self):
        from kplugins.contrib import registry
        # auto_discover_file = getattr(settings, KpluginsK.SS_AUTODISCOVER_FILE.value, 'contrib.kplugins.plugins')
        # autodiscover_modules(auto_discover_file)
