from django.template.loader import render_to_string
from django.utils.safestring import mark_safe
from collections import defaultdict

from kplugins.models import Holder
from kplugins.core.registry.register import Register


class HolderUtils:

    def __init__(self, site, request):
        self.site = site
        self.request = request

    def prepare(self, identifier, holder_plugin_pks=None):
        holder = Holder.objects.get_or_create(site=self.site, identifier=identifier)[0]
        holder_plugin_qs = holder.holderplugin_set.filter(is_active=True).order_by('sort')

        holder_plugin_pks = holder_plugin_pks or []
        if holder_plugin_pks:
            holder_plugin_qs = holder_plugin_qs.filter(pk__in=holder_plugin_pks)

        xs = []
        for obj in holder_plugin_qs:
            val = Register.get_resources()[obj.plugin.identifier]
            plugin_class = val.get('plugin_class', None)
            if plugin_class:
                plugin = plugin_class(self.site, self.request).prepare(options=obj.options)
                xs.append(plugin)
        return {
            'holder': holder,
            'plugins': xs
        }


class HolderRender:

    def __init__(self, request):
        self.request = request

    def build_context(self, plugin_info):
        return {'holder': plugin_info['holder'], 'plugins': plugin_info['plugins']}

    def render_html(self, plugin_info):
        html = render_to_string('kplugins/render/to_html.html', context=self.build_context(plugin_info),
                                request=self.request)
        return mark_safe(html)

    def render_media(self, plugin_info):
        dt = defaultdict(list)
        # Loop through media files (eg: css, js) and group them, remove duplicates.
        for plugin in plugin_info['plugins']:
            for media_type, values in plugin.get_media().items():
                for val in values:
                    if val not in dt[media_type]:
                        dt[media_type].append(val)

        context = self.build_context(plugin_info)
        context['media'] = dict(dt)
        html = render_to_string('kplugins/render/to_media.html', context=context, request=self.request)
        return mark_safe(html)
