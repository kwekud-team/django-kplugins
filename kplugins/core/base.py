from django.template.loader import render_to_string
from django.utils.safestring import mark_safe


class BasePlugin:
    name = ''
    template_name = ''
    options = {}

    def __init__(self, site, request):
        self.site = site
        self.request = request

    def get_context(self):
        return {}

    def get_media(self):
        return {}

    def prepare(self, options=None):
        self.options = options or {}
        return self

    def render(self):
        html = render_to_string(self.template_name, context=self.get_context(), request=self.request)
        return mark_safe(html)


