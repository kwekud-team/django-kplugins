from kplugins.models import Plugin
from kplugins.core.registry.register import Register


class RegisterSaver:

    def __init__(self, site):
        self.site = site

    def process(self):
        Plugin.objects.filter(site=self.site).update(is_active=False)

        for key, val in Register.get_resources().items():
            self._create_plugin(key, val)

    def _create_plugin(self, identifier, val):
        return Plugin.objects.update_or_create(
            site=self.site,
            identifier=identifier,
            defaults={
                'module_path': val['module_path'],
                'name': val['plugin_class'].name,
                'template_name': val['plugin_class'].template_name,
                'is_active': True
            })[0]
