from collections import defaultdict

# _REGISTRY = {}
_REGISTRY = defaultdict(dict)


class Register:

    @staticmethod
    def get_resources():
        return _REGISTRY


def register(module_path, plugin_class):
    cls_path = f'{module_path}.{plugin_class.__name__}'
    _REGISTRY[cls_path] = {
        'module_path': module_path,
        'plugin_class': plugin_class
    }