from dataclasses import dataclass


@dataclass
class RenderResponse:
    is_valid: bool
    message: str
    response: object = None
