import uuid
from django.db import models


class BaseModel(models.Model):
    guid = models.UUIDField()
    site = models.ForeignKey('sites.Site', on_delete=models.CASCADE)
    is_active = models.BooleanField(default=True)
    date_created = models.DateTimeField(auto_now_add=True)
    date_modified = models.DateTimeField(auto_now=True)

    class Meta:
        abstract = True

    def save(self, *args, **kwargs):
        self.guid = self.guid or uuid.uuid4()
        return super().save(*args, **kwargs)


class Plugin(BaseModel):
    identifier = models.CharField(max_length=250)
    module_path = models.CharField(max_length=250)
    name = models.CharField(max_length=100)
    template_name = models.CharField(max_length=250)
    kwargs = models.JSONField(default=dict)

    def __str__(self):
        return self.identifier

    class Meta:
        # ordering = ('sort',)
        unique_together = ('site', 'identifier')


class Holder(BaseModel):
    identifier = models.CharField(max_length=100)

    def __str__(self):
        return self.identifier

    class Meta:
        # ordering = ('sort',)
        unique_together = ('site', 'identifier')


class HolderPlugin(BaseModel):
    holder = models.ForeignKey(Holder, on_delete=models.CASCADE)
    plugin = models.ForeignKey(Plugin, on_delete=models.CASCADE)
    options = models.JSONField(default=dict, blank=True)
    sort = models.PositiveSmallIntegerField(default=1000)

    def __str__(self):
        return f'{self.holder} | {self.plugin}'

    class Meta:
        ordering = ('sort',)
        # unique_together = ('site', 'holder')

    def save(self, *args, **kwargs):
        self.site = self.holder.site
        return super().save(*args, **kwargs)
